# flutter_city_cn

#### 介绍
一个flutter中国城市选择器

#### 软件架构
flutter


#### 使用说明

LocationField 用于显示一个当前位置
SelectCityField 城市选择器页面

1、先将目录下的city.json放入本地资源
2、修改SelectCityField代码中引用城市数据位置，改成你存放city.json的位置

你可以直接打开SelectCityField用来选择城市，也可以通过LocationField的下箭头跳转的选择城市页面
