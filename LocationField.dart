import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'SelectCityField.dart';


class LocationField extends StatefulWidget {
  LocationField({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => LocationFieldState();
}

class LocationFieldState extends State<LocationField> {

  String locationValue = "定位中...";

  void locationText() async{
    await PermissionHandler().requestPermissions([PermissionGroup.location]);
    PermissionStatus permission = await PermissionHandler().checkPermissionStatus(PermissionGroup.location);
    if (permission == PermissionStatus.granted) {
      //这里用高德地图插件获取当前城市，高德地图需要申请相关权限，具体自己百度
      /*
      await AMapLocationClient.startup(new AMapLocationOption( desiredAccuracy:CLLocationAccuracy.kCLLocationAccuracyHundredMeters  ));
      AMapLocation location = await AMapLocationClient.getLocation(true);
      setLocation(location.city);
       */
    }
  }

  void setLocation(String city){
    setState(() {
      if (null != city){
        locationValue = city;
      }
    });
  }

  @override
  void initState(){
    super.initState();
    locationText();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          IconButton(
              color: Colors.grey,
              icon: Icon(Icons.expand_more),
              onPressed: (){
                Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new SelectCityField()
                  ),
                ).then((txt){
                  setLocation(txt);
                });
              }),
          Text(locationValue,
            style: TextStyle(
              color: Colors.black,
              fontSize: 15,
            ),
          ),
        ],
      ),
    );
  }
}
